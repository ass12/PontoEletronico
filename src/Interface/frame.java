package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import controllers.ControleMarcacoes;
import Types.checkOperation;

public class frame extends JFrame implements ActionListener {

    JButton button = new JButton("Cadastrar Novo Usuário");
    JButton button2 = new JButton("Marcações");
    JButton idButton = new JButton("OK");
    JButton consultar = new JButton("Emoções...");
    JTextField idFuncionario = new JTextField();
    JLabel idLabel = new JLabel("Digite o Id do funcionario");

    public frame() {

        button.setBounds(300, 350, 200, 45);
        button.addActionListener(this);

        button2.setBounds(300, 470, 200, 45);
        button2.addActionListener(this);

        idButton.setBounds(500, 250, 55,50);
        idButton.addActionListener(this);

        consultar.setBounds(300,410,200,45);
        consultar.addActionListener(this);

        idLabel.setBounds(300, 215, 200,50);


        idFuncionario.setBounds(300, 250, 200,50);

        this.setTitle("Ponto Digital");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setSize(800, 600);
        this.setVisible(true);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        this.add(button);
        this.add(idFuncionario);
        this.add(consultar);
        this.add(idButton);
        this.add(new clock().relogio);
        this.add(idLabel);
        this.add(button2);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String ID = "";
        checkOperation checkCommit = new checkOperation();
        if (e.getSource() == idButton) {
            ID = idFuncionario.getText();
            try {
              checkCommit = ControleMarcacoes.novaMarcacao(ID);
            } catch (IOException err) {
              new okpanel("algo inesperado aconteceu").ok.addActionListener(f -> {});
            }


             if (checkCommit.getStatus() == true) {

                new okpanel("\n " + checkCommit.getMessage()).ok.addActionListener(f -> {});

            } else {

                new notOkPanel("\n " + checkCommit.getMessage()); //vrau

            }

        }

        if(e.getSource()==button){
            new tela2();
        }

        if (e.getSource() == consultar){

            new okpanel("                        ...Finais :)");

        }

        if (e.getSource() == button2){

            new Intermediaria(2);

        }


    }
}

