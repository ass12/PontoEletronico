package Interface;

import javax.swing.*;

public class notOkPanel extends JFrame {

    JButton notok = new JButton("OK");

    notOkPanel(String error) {
        String message = "Erro: " + error;

        notok.setBounds(80, 80, 75, 50);

        JLabel success = new JLabel(message);
        success.setBounds(30, 10, 200, 75);

        this.setLayout(null);
        this.setSize(250, 250);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        this.add(notok);
        this.add(success);

        notok.addActionListener(e -> {
            this.dispose();
        });

    }

}
