package Interface;

import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Consulta extends JFrame  {

    JButton voltar = new JButton("Voltar");
    JButton submit = new JButton("submit");

    JTextField nome = new JTextField();
    JTextField pesquisa = new JTextField();

    JLabel pesquisaLabel = new JLabel("Nome do funcionário");
    JLabel nomeLabel = new JLabel("Consulta");

    JTable tabelaFuncionarios;

    JScrollPane slide;

    private TableRowSorter sorter;

    private TableModel model;

    Consulta(ArrayList<ArrayList<String>> marc){

        tabela2(marc);
        this.pesquisaLabel.setText("Filtrar por dia (mm dd)");
        this.setTitle("MARCAÇÕES");
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setSize(800, 600);
        this.setLayout(null);
        this.setVisible(true);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        tabelaFuncionarios.setEnabled(false);


        this.add(pesquisa);
        this.add(pesquisaLabel);
        this.add(slide);
        this.add(submit);
        this.add(voltar);
        this.add(nome);
        this.add(nomeLabel);

        voltar.addActionListener(e -> {
          this.dispose();
        });


      }

      public void tabela(){
        String[] columnNames = { "Data de admissão",
                "Nome do Colaborador",
        };

        Object[][] rowData = {
                { "27/05/2023", "José da Silva" }
        };

        tabelaFuncionarios = new JTable(rowData, columnNames);
        slide = new JScrollPane(tabelaFuncionarios);

        pesquisa.setBounds(250, 50, 300, 20);

        pesquisaLabel.setBounds(250, 20, 300, 40);

        voltar.setBounds(50, 500, 150, 50);

        model = new DefaultTableModel(rowData, columnNames);
        sorter = new TableRowSorter<>(model);
        slide.setBounds(250, 150, 300, 500);
        tabelaFuncionarios.setFillsViewportHeight(true);
        slide.setOpaque(true);
        tabelaFuncionarios.setRowSorter(sorter);

        pesquisa.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            public void search(String str) {
                if (str.length() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    sorter.setRowFilter(RowFilter.regexFilter(str));
                }
            }
        });
      }
      public void tabela2(ArrayList<ArrayList<String>> marc){
        String[] columnNames = { "Data",
                "Marcacao",
        };

        Object[][] rowData = {
                { marc.get(0).get(0), marc.get(1).get(0) },
        };

        DefaultTableModel model = new DefaultTableModel(rowData, columnNames);
        if (marc.get(0).size() > 1) {
          for (int i = 1; i <= marc.size(); i++) {
            model.addRow(new Object[]{marc.get(0).get(i), marc.get(1).get(i)});
          }
        }
        tabelaFuncionarios = new JTable(model);

        slide = new JScrollPane(tabelaFuncionarios);

        pesquisa.setBounds(180, 70, 180, 20);

        pesquisaLabel.setBounds(180, 40, 300, 40);

        voltar.setBounds(30, 500, 100, 40);

        sorter = new TableRowSorter<>(model);
        slide.setBounds(180, 100, 450, 500);
        tabelaFuncionarios.setFillsViewportHeight(true);
        slide.setOpaque(true);
        tabelaFuncionarios.setRowSorter(sorter);

        pesquisa.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                search(pesquisa.getText());
            }

            public void search(String str) {
                if (str.length() == 0) {
                    sorter.setRowFilter(null);
                } else {
                    sorter.setRowFilter(RowFilter.regexFilter(str));
                }
            }
        });
      }
}
