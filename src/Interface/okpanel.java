package Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class okpanel extends JFrame implements ActionListener {

    JButton ok = new JButton("OK");

    okpanel(String messageErr) {
        ok.setBounds(80, 80, 75, 50);

        JLabel success = new JLabel(messageErr);
        success.setBounds(20, 10, 200, 75);

        this.setLayout(null);
        this.setSize(250, 250);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        this.add(ok);
        this.add(success);

        ok.addActionListener(e -> {
            this.dispose();
        });

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        this.dispose();
    }

}
