package Interface;

import javax.swing.*;

import controllers.ControleMarcacoes;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Intermediaria extends JFrame implements ActionListener {

    JTextField nameField = new JTextField();
    JLabel nameLabel = new JLabel("Digite o mes e ano (yyyy mm)");
    JTextField nameField2 = new JTextField();
    JLabel nameLabel2 = new JLabel("Digite o Id do funcionario");
    JButton submit = new JButton("Submit");
    private int switchInterface;

    Intermediaria(int switchInterface){
        this.switchInterface = switchInterface;
        nameLabel2.setBounds(50, 25, 170,50);
        nameField2.setBounds(50,60,170,45);
        nameLabel.setBounds(50, 95, 170,50);
        nameField.setBounds(50,130,170,45);
        submit.setBounds(50,200,170,45);


        this.setTitle("Nome");
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setLayout(null);
        this.setSize(285, 300);
        this.setVisible(true);
        this.setResizable(false);
        this.setLocationRelativeTo(null);

        submit.addActionListener(this);
        nameField.addActionListener(this);

        this.add(nameField);
        this.add(nameLabel);
        this.add(nameField2);
        this.add(nameLabel2);
        this.add(submit);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String ID = "";
        String ID2 = "";


          if (e.getSource() == submit) {
            ID = nameField.getText();
            ID2 = nameField2.getText();
            if (ID.isEmpty() || ID2.isEmpty()) {
              new notOkPanel("Preencha todos os campos!");
              return;
            }

          if (switchInterface == 1) {
            //futura tela de consulta :)
          } else if (switchInterface == 2) {
            try {
              final ArrayList<ArrayList<String>> data = ControleMarcacoes.getMarcacoesDia(ID, ID2);
              if(data.get(0).isEmpty()) {
                throw new IOException("Data invalida");
              }

              new Consulta(data);
              this.dispose();
            } catch (IOException er) {
              if (er.getMessage() == "Data invalida") {
                new notOkPanel(er.getMessage());

              } else {
                new notOkPanel("Algo inesperado aconteceu.");
              }
            }
          }
        }
    }

}
