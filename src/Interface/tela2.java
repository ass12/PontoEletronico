package Interface;

import javax.swing.*;
import Types.checkOperation;
import controllers.ControleFuncionario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class tela2 extends JFrame implements ActionListener {
    private String test;
    JButton submit = new JButton("Submit");
    JButton voltar = new JButton("Tela anterior");
    JTextField nameField = new JTextField();
    JTextField ingressField = new JTextField();
    JTextField cpfField = new JTextField();
    JTextField idField = new JTextField();

    tela2() {

                          // cria os text fields e define o texto no campo
        nameField.setBounds(250, 101, 300, 30); // define o tamanho e a posição
        JLabel nameLabel = new JLabel("Name");               // cria uma label
        nameLabel.setBounds(250, 75, 300, 30);

        ingressField.setBounds(250, 155, 300, 30);
        JLabel ingressLabel = new JLabel("Ingress Date (yyy/mm/dd)");
        ingressLabel.setBounds(250, 131, 300, 30);

        cpfField.setBounds(250, 210, 300, 30);
        JLabel cpfLabel = new JLabel("CPF");
        cpfLabel.setBounds(250, 186, 300, 30);

        idField.setBounds(250, 265, 300, 30);
        idField.setEditable(false);
        JLabel idLabel = new JLabel("Worker ID");
        idLabel.setBounds(250, 241, 300, 30);

        submit.setBounds(250, 300, 300, 30);
        submit.addActionListener(this);

        voltar.setBounds(50,500,150,50);

        this.setTitle("Cadastro Funcionario");                  //Cria tela "cadastro funcionario"
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setLayout(null);
        this.setSize(800, 600);
        this.setVisible(true);
        this.setResizable(false); //
        this.setLocationRelativeTo(null);

        this.add(nameField);                                    //adiciona à tela principal os textFields
        this.add(ingressField);
        this.add(cpfField);
        this.add(idField);
        this.add(nameLabel);
        this.add(ingressLabel);
        this.add(cpfLabel);
        this.add(idLabel);
        this.add(submit); //
        this.add(voltar);

        voltar.addActionListener(e -> {
            this.dispose();
        });




    }

    tela2(String name, String date) {
        nameField.setText(name);
        System.out.println(name);
                          // cria os text fields e define o texto no campo
        nameField.setBounds(250, 101, 300, 30); // define o tamanho e a posição
        JLabel nameLabel = new JLabel("Name");               // cria uma label
        nameLabel.setBounds(250, 75, 300, 30);

        ingressField.setBounds(250, 155, 300, 30);
        JLabel ingressLabel = new JLabel("Ingress Date (yyy/mm/dd)");
        ingressLabel.setBounds(250, 131, 300, 30);

        cpfField.setBounds(250, 210, 300, 30);
        JLabel cpfLabel = new JLabel("CPF");
        cpfLabel.setBounds(250, 186, 300, 30);

        idField.setBounds(250, 265, 300, 30);
        idField.setEditable(false);
        JLabel idLabel = new JLabel("Worker ID");
        idLabel.setBounds(250, 241, 300, 30);

        submit.setBounds(250, 300, 300, 30);
        submit.addActionListener(this);

        voltar.setBounds(50,500,150,50);

        this.setTitle("Cadastro Funcionario");                  //Cria tela "cadastro funcionario"
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setLayout(null);
        this.setSize(800, 600);
        this.setVisible(true);
        this.setResizable(false); //
        this.setLocationRelativeTo(null);

        this.add(nameField);                                    //adiciona à tela principal os textFields
        this.add(ingressField);
        this.add(cpfField);
        this.add(idField);
        this.add(nameLabel);
        this.add(ingressLabel);
        this.add(cpfLabel);
        this.add(idLabel);
        this.add(submit); //
        this.add(voltar);

        voltar.addActionListener(e -> {
            this.dispose();
        });




    }

    @Override
    public void actionPerformed(ActionEvent e) { //detecta o click dos Jbuttons


        String ID1 = "";
        String ID2 = "";
        String ID3 = "";
        checkOperation checkCommit = new checkOperation();

        if (e.getSource() == submit) {
          ID1 = nameField.getText();
          ID2 = ingressField.getText();
          ID3 = cpfField.getText();


            //debug
            System.out.println(ID1);
            System.out.println(ID2);
            System.out.println(ID3);
            try {
              checkCommit = ControleFuncionario.novoFuncionario(ID1, ID2, ID3);

            } catch (IOException err) {
              new notOkPanel("Algo inesperado aconteceu!");
            }
            System.out.println(checkCommit.getStatus()); //degug

            if (checkCommit.getStatus() == true) {

                new okpanel(checkCommit.getMessage()).ok.addActionListener(f -> {
                    this.dispose();
                });

            } else {
              new notOkPanel(checkCommit.getMessage()); //vrau

            }
        }

    }
  }
