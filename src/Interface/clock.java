package Interface;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class clock implements Runnable {

    Thread t = null;
    int hours = 0, minutes = 0, seconds = 0;
    String timeString = "";
    public JButton relogio;

    clock() {
        relogio = new JButton();
        relogio.setBounds(300, 100, 200, 100);

        t = new Thread(this);
        t.start();
    }

    public void run() {
        try {
            while (true) {

                Calendar cal = Calendar.getInstance();
                hours = cal.get(Calendar.HOUR_OF_DAY);
                if (hours > 12)
                    hours -= 12;
                minutes = cal.get(Calendar.MINUTE);
                seconds = cal.get(Calendar.SECOND);

                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
                Date date = cal.getTime();
                timeString = formatter.format(date);

                printTime();

                Thread.sleep(1000);  // interval given in milliseconds
            }
        } catch (Exception e) {
        }
    }

    public void printTime() {
        relogio.setText(timeString);
    }
}
