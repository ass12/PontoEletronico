package repository;

import Types.Marcacoes;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;

public class repositoryMarcacoes {
  private static Path path = Path.of(".\\dataBase\\users\\");

  public static boolean createTimeCard(Marcacoes marcacao) throws IOException {
    String newLine = marcacao.getDateYMD();
    ArrayList<String> marcDia = marcacao.getMarcDia();

    for (String element : marcDia) {
      newLine += "," + element;
    }

    newLine += ";";

    String userList = Files.readString(path.resolve(marcacao.getId() + "\\timeCard.txt"));
    String[] userListArray = userList.split(";");
    String newMarcFile = new String("");

    String[] itemItems;
    boolean editing = false;
    for (String item : userListArray) {
      if (item != "") {
        itemItems = item.split(",");
        if (!(itemItems[0].equals(marcacao.getDateYMD()))) {
          newMarcFile += item + ";";
        } else {
          newMarcFile += newLine;
          editing = true;
        }
      }
    }

    if(editing) {
      Files.write(path.resolve(marcacao.getId() + "\\timeCard.txt"), (newMarcFile).getBytes(StandardCharsets.UTF_8));
    } else {
      Files.write(path.resolve(marcacao.getId() + "\\timeCard.txt"), (newLine).getBytes(StandardCharsets.UTF_8),
        StandardOpenOption.APPEND);
    }
    return true;
  }

  public static boolean readTimeCard() {
    return true;
  }

  public static Marcacoes findTimeCard(String id, String monthYear) throws IOException {
    String timeCardList = Files.readString(path.resolve(id + "\\timeCard.txt"));
    String[] userListArray = timeCardList.split(";");

    String marcacoesString = null;

    for (String item : userListArray) {
      if (item.split(",")[0].equals(monthYear)) {
        marcacoesString = item;
      }
    }

    if (marcacoesString == null) {
      return null;
    } else {
      String[] marcacoesArray = marcacoesString.split(",");
      ArrayList<String> marcDia = new ArrayList<String>();
      for (int i = 1; i < marcacoesArray.length; i++) {
        marcDia.add(marcacoesArray[i]);
      }
      return new Marcacoes(marcacoesArray[0], id, marcDia);
    }

  }

  public static boolean editTimeCard() {
    // Em breve em suas melhores emoções :)
    return true;
  }
}
