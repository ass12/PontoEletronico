package repository;

import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Comparator;
import java.util.stream.Stream;
import java.io.IOException;
import Types.Funcionario;

public class repositoryFuncionario {
  private static Path path = Path.of("./dataBase/users/");
  private static Path userListPath = path.resolve("userList.txt");

  public static Funcionario createUser(Funcionario user) {
    try {
      int newId = getNewId();
      createUserDirectory(newId+"");
      addToUserList(user.getName(), newId, user.getCpf(), user.getDataAdmissao());

      return new Funcionario(user.getName(), user.getDataAdmissao(), user.getCpf(), newId + "");
    } catch (Exception err) {
      return null;
    }
  }

  public static boolean removeUser(int id) throws IOException {
    String userList = Files.readString(userListPath);
    String[] userListArray = userList.split(";");
    String newUsersFile = new String(";");

    String[] itemItems;
    for (String item : userListArray) {
      if (item != "") {
        itemItems = item.split(",");
        if (!(Integer.parseInt(itemItems[0]) == id)) {
          newUsersFile += item + ";";
      }
    }
    Files.write(userListPath, newUsersFile.getBytes(StandardCharsets.UTF_8));

      try (Stream<Path> walk = Files.walk(path.resolve(id + ""))) {
        walk.sorted(Comparator.reverseOrder()).forEach(path -> {
          try {
            Files.delete(path);
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
      }
    }

    return true;
  }

  public static Boolean doesExistId(String id) throws IOException {
    String userListString = Files.readString(userListPath);
    if(userListString.equals("")) return false;
    String[] userListArray = userListString.split(";");

    for (int i = 0; i < userListArray.length; i++) {
      if (userListArray[i].split(",")[0].equals(id)) {
        return true;
      }
    }

    return false;
  }

  public static Boolean doesExistCpf(String Cpf) throws IOException {
    String userListString = Files.readString(userListPath);
    if(userListString.equals("")) return false;
    String[] userListArray = userListString.split(";");

    for (int i = 0; i < userListArray.length; i++) {
      if (userListArray[i].split(",")[2].equals(Cpf)) {
        return true;
      }
    }
    return false;
  }

  private static boolean createUserDirectory(String id) {
    try {
      Path newUserDirectory = Files.createDirectories(path.resolve(id));
      Files.createFile(newUserDirectory.resolve("timeCard.txt"));
      return true;
    } catch (IOException err) {
      return false;
    }

  }

  private static boolean addToUserList(String name, int id, String cpf, String dataAdmissao) {
    String newLine = id + "," + name + "," + cpf + "," + dataAdmissao + ";";
    try {
      Files.write(path.resolve("userList.txt"), (newLine).getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
      return true;
    } catch (IOException err) {
      System.out.println(err);
      return false;
    }

  }

  private static int getNewId() {
    try {
      String userList = Files.readString(path.resolve("userList.txt"));
      if (userList.equals("")) {
        return 1;
      } else {
        String[] userListArray = userList.split(";");
        System.out.println(userListArray.length);
        System.out.println(userList);

        int lastId = Integer.parseInt(userListArray[userListArray.length - 1].split(",")[0]);
        return lastId + 1;
      }
    } catch (IOException err) {
      System.out.println(err);
      return 0;
    }
  }
}
