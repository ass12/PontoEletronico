package Types;

public class checkOperation {
  private Boolean status;
  private String message;

  public checkOperation() {}
  public checkOperation(Boolean status, String message) {
    this.status = status;
    this.message = message;
  }

  public String getMessage() {
      return message;
  }

  public Boolean getStatus() {
      return status;
  }
}
