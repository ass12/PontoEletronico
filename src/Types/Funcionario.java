package Types;

public class Funcionario {
  private String name;
  private String dataAdmissao;
  private String cpf;
  private String id;

  public Funcionario() {}

  public Funcionario(String name, String dataAdmissao, String cpf, String id) {
    this.name = name;
    this.dataAdmissao = dataAdmissao;
    this.cpf = cpf;
    this.id = id;
  }

  public Funcionario(String name, String dataAdmissao, String cpf) {
    this.name = name;
    this.dataAdmissao = dataAdmissao;
    this.cpf = cpf;
  }

  public String getName() {
    return name;
  }

  public String getCpf() {
    return cpf;
  }

  public String getDataAdmissao() {
    return dataAdmissao;
  }
  public String getId() {
    return id;
  }
  public void printData() {
    System.out.println(name);
    System.out.println(dataAdmissao);
    System.out.println(cpf);
    System.out.println(id);

    System.out.println("");
  }
}
