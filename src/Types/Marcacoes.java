package Types;

import java.util.ArrayList;

public class Marcacoes {
  //substituir DateYMD e year por DateYMDYear (ver com o felipe)
  public String DateYMD;
  public String id;
  public ArrayList<String> MarcDia = new ArrayList<String>();

  public Marcacoes() {}

  public String getDateYMD() {
    return DateYMD;
  }

  public String getId() {
    return id;
  }

  public Marcacoes(String DateYMD, String id, ArrayList<String> MarcDia) {
    this.DateYMD = DateYMD;
    this.id        = id;
    this.MarcDia.addAll(MarcDia);
  }

  public ArrayList<String> getMarcDia() {
      return MarcDia;
  }

  public void printData() {
    System.out.println(DateYMD);
    System.out.println(id);

    for (String string : MarcDia) {
      System.out.printf("%s ", string);
    }

    System.out.println("\n");
  }
}
