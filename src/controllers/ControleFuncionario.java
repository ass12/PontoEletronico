package controllers;

import java.io.IOException;
import Types.Funcionario;
import Types.checkOperation;
import repository.repositoryFuncionario;

public class ControleFuncionario {
  public static checkOperation novoFuncionario(String nome, String dataAdmissao, String Cpf) throws IOException {
    if (Cpf.isEmpty() || nome.isEmpty() || dataAdmissao.isEmpty()) {
      return new checkOperation(false, "preencha os campos!");
    }

    Boolean cpfValido = repositoryFuncionario.doesExistCpf(Cpf);

    if(cpfValido) {
      return new checkOperation(false, "cpf ja cadastrado!");
    }

    Funcionario novoFuncionario = new Funcionario(nome, dataAdmissao, Cpf);
    final Funcionario newUser = repositoryFuncionario.createUser(novoFuncionario);
    newUser.printData();
    return new checkOperation(true, ("Funcionario cadastrado, id: " + newUser.getId()));
  }

  public static Boolean removeFuncionario(String id) throws IOException {
    Boolean ExistById = repositoryFuncionario.doesExistId(id);

    if(!ExistById) {
      return false;
    }

    Boolean status = repositoryFuncionario.removeUser(Integer.parseInt(id));
    return status;
  }
}
