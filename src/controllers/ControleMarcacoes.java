package controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import Types.Marcacoes;
import Types.checkOperation;
import repository.repositoryFuncionario;
import repository.repositoryMarcacoes;

public class ControleMarcacoes {

  public static ArrayList<ArrayList<String>> getMarcacoesDia(String yearMonth, String id) throws IOException {
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> hours = new ArrayList<>();
    String completeMarc = "";
    for (int i = 1; i <= 31; i++) {
      String spaceZero = " 0";

      if (i > 9) {
        spaceZero = " ";
      }

      String completeDate = yearMonth + spaceZero + i;
      final Marcacoes marc = repositoryMarcacoes.findTimeCard(id, completeDate);
      completeDate = "";
      if (marc == null) {
        // throw new IOException("Data invalida");
      } else {
          date.add(marc.DateYMD);

          for (String string : marc.getMarcDia()) {
            completeMarc += " " + string;
          }
          hours.add(completeMarc);
          completeMarc = "";
        }
      }
      ArrayList<ArrayList<String>> arr = new ArrayList<>();
      arr.add(date);
      arr.add(hours);

    return arr;
  }
  public static checkOperation novaMarcacao(String id) throws IOException {
    LocalDateTime timeNow = LocalDateTime.now();
    checkOperation bool = verificaMarcacao(timeNow, id);
    return bool;

 }

  private static checkOperation verificaMarcacao(LocalDateTime newMarc, String id) throws IOException {
    Boolean existId = repositoryFuncionario.doesExistId(id);

    //verifica se existe id
    if (!existId) {
      return new checkOperation(false, "Id não existe");
    }

    String monthYearDay = getMonthYearDay(newMarc);

    //nao existe dia
    if (repositoryMarcacoes.findTimeCard(id, monthYearDay) == null) {
      repositoryMarcacoes.createTimeCard(new Marcacoes(monthYearDay, id, new ArrayList<String>()));
    }

    final Marcacoes marc = repositoryMarcacoes.findTimeCard(id, monthYearDay);

    int index = marc.getMarcDia().size();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    if (index > 0) {
      LocalTime lastMarc = LocalTime.parse(marc.getMarcDia().get(index -1), formatter);
      int lastMarcSecond = getSeconds(lastMarc);
      int newMarcSecond  = getSeconds(newMarc);

      if (testTolerancia(lastMarcSecond, newMarcSecond)) {
        return new checkOperation(false, "Maracao ja registrada");
      }
    }

    String marcUnit = newMarc.format(formatter);
    marc.getMarcDia().add(marcUnit);
    repositoryMarcacoes.createTimeCard(marc);
    return new checkOperation(true, "Marcacao registrada!");
  }

  private static Boolean testTolerancia(int lastMarcSecond, int newMarcSecond) {
    int diff = lastMarcSecond - newMarcSecond;

    //testa se o tempo entre a ultima batida e a atual é de 5min de diferença
    if (diff <= 300 && diff >= -300) {
      return true;
    } else {
      return false;
    }

  }

  private static int getSeconds(LocalDateTime dateTime) {
    LocalTime time = dateTime.toLocalTime();
    return time.toSecondOfDay();
  }

  private static int getSeconds(LocalTime time) {
    return time.toSecondOfDay();
  }

  private static String getMonthYearDay(LocalDateTime dateTime) {
    LocalDate monthYearDay = dateTime.toLocalDate();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd");
    return monthYearDay.format(formatter);
  }

}
